<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactoLead extends Model
{
    protected $guarded = [];
    protected $hidden = [];

    public function relatedLead()
    {
        return $this->belongsTo(Leads::class, 'lead_id');
    }
}
