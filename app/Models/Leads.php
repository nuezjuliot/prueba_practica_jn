<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Leads extends Model
{
    protected $guarded = [];
    protected $hidden = [];

    public function relatedContacto()
    {
        return $this->hasMany(ContactoLead::class, 'lead_id');
    }
}
