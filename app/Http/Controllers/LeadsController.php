<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\leads;
use DB;

class LeadsController extends Controller
{

    public function index(Leads $leads)
    {
        try {
            return $leads->with('relatedContacto')->get();
        } catch (Exception $e) {
            throw new Exception($e, 1);
            
        }
    }
    public function store(Request $request,Leads $leads)
    {
        try {
            return DB::transaction(function() use ($leads, $request){

                $leads->nombres = $request->nombres;
                $leads->apellidos = $request->apellidos;
                $leads->email = $request->email;
                $leads->teléfono = $request->teléfono;
                $leads->programa = $request->programa;
                $leads->save();

                return Response(['mensaje' => 'Guardado con exito']);
           }, 5);
        } catch (Exception $e) {
            
            throw new Exception($e, 1);
            
        }
    }
}
