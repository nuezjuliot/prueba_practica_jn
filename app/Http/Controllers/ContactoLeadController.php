<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ContactoLead;
use DB;

class ContactoLeadController extends Controller
{
    public function store(ContactoLead $contacto, Request $request)
    {
        try {
            return DB::transaction(function () use ($request, $contacto){

                $contacto->nota = $request->nota;
                $contacto->contactado = true;
                $contacto->lead_id = $request->lead_id;
                $contacto->save();
                return ['mensaje' => 'contacto guardado con exito'];
            }, 5);
        } catch (Exception $e) {
            throw new Exception($e, 1);
        }
    }
}
