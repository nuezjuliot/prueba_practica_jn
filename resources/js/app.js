require('./bootstrap');

window.Vue = require('vue');
import Vue from 'vue'
import Vuetify from 'vuetify'
import VueSweetalert2 from 'vue-sweetalert2';
import 'vuetify/dist/vuetify.min.css'
import 'sweetalert2/dist/sweetalert2.min.css';

Vue.use(Vuetify)
Vue.use(VueSweetalert2);

Vue.component('leads-component', require('./components/leads.vue').default);
Vue.component('leads-administration-component', require('./components/leadsAdministration.vue').default);

const app = new Vue({
    el: '#app',
    vuetify: new Vuetify({})
});
